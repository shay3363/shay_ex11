#include "threads.h"



int main()
{
	call_I_Love_Threads();
	cout << "call get Primes 0-1000:" << endl;
	vector<int> primes3 = callGetPrimes(0, 1000);
	cout << "call get Primes 0-100000:" << endl;
	primes3 = callGetPrimes(0, 100000);
	cout << "call get Primes 0-1000000:" << endl;
	primes3 = callGetPrimes(0, 1000000);
	cout << "call get Primes to file:" << endl;
	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 2);
	system("pause");
	return 0;
}