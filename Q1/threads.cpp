#include "threads.h"
std::mutex mu;
void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}
void call_I_Love_Threads()
{
	thread myPrint(I_Love_Threads);
	myPrint.join();
}
void printVector(vector<int> primes)
{
	for (vector<int>::iterator i = primes.begin(); i < primes.end(); i++)
	{
		cout << *i << endl;
	}
}
bool isPrime(int num)
{
	bool flag = true;
	int max = sqrt(num);
	int i = 0;
	for (i = 2; i < max && flag; i++)
	{
		if (num%i == 0)
		{
			flag = false;
		}
	}
	return flag;
}
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = 0;
	for (i = begin; i <= end; i++)
	{
		if (isPrime(i))
		{
			primes.push_back(i);
		}
	}
}
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> newVector = vector<int>();
	clock_t tStart = clock();
	thread primes(getPrimes,ref(begin),ref(end),ref(newVector));
	primes.join();
	cout << "Time taken: " << (double)(clock() - tStart) / CLOCKS_PER_SEC << endl;
	return newVector;
}
void writePrimesToFile(int begin, int end, ofstream& file)
{
	string s = "";
	int i = 0;
	for (i = begin; i <= end; i++)
	{
		if (isPrime(i))
		{
			s = to_string(i);
			mu.lock();
			file << s << '\n';
			mu.unlock();
		}
	}
}
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int miniRange = (end - begin) / N;
	int startNewRange = begin;
	int endNewRange = begin+miniRange;
	int i = 0;
	ofstream myFile;
	myFile.open(filePath);
	ofstream& fileRef = myFile;
	vector<thread> primesToFile = vector<thread>();
	vector<thread>::iterator endVector;
	clock_t tStart = clock();
	for (i = 0; i < N; i++)
	{
		primesToFile.push_back(thread(writePrimesToFile, ref(startNewRange), ref(endNewRange), ref(myFile)));
		startNewRange = endNewRange + 1;
		endNewRange += miniRange;
	}
	endVector = primesToFile.end();
	for (vector<thread>::iterator i = primesToFile.begin(); i < endVector; i++)
	{
		i->join();
	}
	cout << "Time taken: " << (double)(clock() - tStart) / CLOCKS_PER_SEC << endl;
}